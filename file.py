from osfile import OsFile


class File(OsFile):
    def __init__(self, full_path, directory):
        super().__init__(full_path, directory)
        self.update_md5()

    def scan(self):
        if self.exists():
            self._is_edited()
        else:
            self.scanner_alerts.deleted()
            self._remove_from_directory()

    def _get_md5(self):
        md5 = super()._get_md5()

        buf_size = 65536  # lets read stuff in 64kb chunks!

        with open(self.path, 'rb') as f:
            while True:
                data = f.read(buf_size)
                if not data:
                    break
                md5.update(data)
        return md5

    def _remove_from_directory(self):
        self.directory.files.remove(self)
