from scanner_alerts import ScannerAlerts
import hashlib
import abc
import os


class OsFile(object):
    def __init__(self, full_path, directory):
        self.path = os.path.abspath(full_path)
        self.name = os.path.basename(self.path)
        self.directory = directory
        self._md5 = hashlib.md5()
        self.scanner_alerts = ScannerAlerts(self)

    @abc.abstractmethod
    def _get_md5(self):
        """:returns md5 object"""
        md5 = hashlib.md5()
        md5.update(self.name.encode('utf-8'))
        return md5

    def get_md5(self):
        """:returns md5 string"""
        return self._md5.hexdigest()

    @abc.abstractmethod
    def scan(self):
        return

    def update_md5(self):
        self._md5 = self._get_md5()

    def exists(self):
        return os.path.exists(self.path)

    def _change_alert(self):
        # if the directory is not the root directory
        if self.directory is not None:
            self.directory._is_edited()

    def _is_edited(self):
        if self.get_md5() != self._get_md5().hexdigest():
            self.scanner_alerts.changed()
            self.update_md5()
            self._change_alert()
