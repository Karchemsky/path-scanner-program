from directory import Directory
import time


def main():
    try:
        paths = Directory.get_directory_path()

        my_directories = []

        for every_path in paths:
            my_directories.append(Directory(every_path, None))

        print("The scanning has started.")

        while True:
            time.sleep(2.5)
            for every_directory in my_directories:
                every_directory.scan()

    except KeyboardInterrupt:
        print("The scanning has stopped.")


if __name__ == '__main__':
    main()
