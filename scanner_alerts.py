class ScannerAlerts:
    def __init__(self, os_file):
        self.os_file = os_file

    def deleted(self):
        alert_message = "{0} {1} was deleted.".format(self.os_file.__class__.__name__, self.os_file.name)
        print(alert_message)

    def changed(self):
        alert_message = "{0} {1} was changed.".format(self.os_file.__class__.__name__, self.os_file.name)
        print(alert_message)

    @staticmethod
    def added_os_file(type_, name):
        alert_message = "{0} {1} was added.".format(type_, name)
        print(alert_message)
