from osfile import OsFile
from file import File
import os


class Directory(OsFile):
    def __init__(self, full_path, directory):
        super().__init__(full_path, directory)
        self.files, self.directories = [], []
        self.update_files()
        self.update_directories()
        self.update_md5()

    @staticmethod
    def get_directory_path():
        valid_path = False
        path = ""
        directories_paths = ""
        while not valid_path:
            path += input("Please enter full paths of directories to scan, separated by spaces:\n")

            paths = path.split(' ')
            if Directory._is_valid_paths(paths):
                directories_paths = paths
                valid_path = True

        return Directory._reducing_paths(directories_paths)

    @staticmethod
    def _reducing_paths(paths):
        new_paths = []
        for path in paths:
            if not any(every_path in path for every_path in new_paths):
                new_paths.append(path)
        return new_paths

    @staticmethod
    def _is_valid_paths(paths):
        for every_path in paths:
            if not os.path.isdir(every_path):
                print("One or more of the paths doesn't exists, Try again.")
                return False
        return True

    def _get_md5(self):
        md5 = super()._get_md5()
        for every_os_file in self.files + self.directories:
            md5.update(every_os_file._md5.hexdigest().encode())
        return md5

    def update_files(self):
        self.files = self._get_files_and_directories()[0]

    def update_directories(self):
        self.directories = self._get_files_and_directories()[1]

    def _get_files_and_directories(self):
        all_files = os.listdir(self.path)
        files = []
        directories = []
        for every_file in all_files:
            file_name = self.path + '/' + every_file
            if os.path.isdir(file_name):
                directories.append(Directory(file_name, self))
            elif os.path.isfile(file_name):
                files.append(File(file_name, self))
        return files, directories

    def _has_file(self, full_path):
        """Checks if a path for the file is a new file in the directory."""
        paths = [every_file.path for every_file in self.files]
        return full_path in paths

    def _has_directory(self, full_path):
        """Checks if a path for the directory is a new directory in the directory."""
        paths = [every_directory.path for every_directory in self.directories]
        return full_path in paths

    def _scan_all_directories(self):
        for every_directory in self.directories:
            every_directory.scan()

    def _scan_all_files(self):
        for every_file in self.files:
            every_file.scan()

    def scan(self):
        if self.exists():
            self._is_edited()
            self._check_new_os_files()
            self._scan_all_directories()
            self._scan_all_files()
        else:
            self.scanner_alerts.deleted()
            self._remove_from_directory()

    def _remove_from_directory(self):
        self.directory.directories.remove(self)

    def _get_current_files(self):
        all_files = os.listdir(self.path)
        files = []
        directories = []
        for every_file in all_files:
            file_name = self.path + '/' + every_file
            if os.path.isdir(file_name):
                directories.append(file_name)
            elif os.path.isfile(file_name):
                files.append(file_name)
        return files, directories

    def _check_new_os_files(self):
        current_files, current_directories = self._get_current_files()
        for current_os_file in current_files + current_directories:
            if current_os_file not in [regular_file.path for regular_file in self.files]\
                    and os.path.isfile(current_os_file):
                self.files.append(File(current_os_file, self))
                self.scanner_alerts.added_os_file("File", current_os_file)
            elif current_os_file not in [directory_file.path for directory_file in self.directories]\
                    and os.path.isdir(current_os_file):
                self.directories.append(Directory(current_os_file, self))
                self.scanner_alerts.added_os_file("Directory", os.path.basename(current_os_file))
